# Hugo Casildo Test

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```


### Git repository
[GitLab](https://gitlab.com/hugo.casildo/softwarenextdoor-test)

### URL
[URL](https://softwarenextdoor-hugo-casildo.netlify.app/)


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
