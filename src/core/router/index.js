import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '@/components/views/Index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: Index
  },
  {
    path: '/details/:idSerie',
    name: 'details',
    props: true,
    component: () => import(/* webpackChunkName: "detail" */ '@/components/views/Detail.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
