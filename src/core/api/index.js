const URL = process.env.VUE_APP_URL
import axios from 'axios'

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

function getAll() {
  return axios.get(`${URL}/shows`)
    .then( succ => succ.data)
}

function getQuery(query) {
  return axios.get(`${URL}/search/shows?q=${query}`)
    .then( succ => succ.data)
}

function getDetail(id) {
  return axios.get(`${URL}/shows/${id}`)
    .then( succ => succ.data)
}

export default {
  getAll,
  getQuery,
  getDetail,
}
