import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/core/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    series: [],
    serieSelected: {},
    pages: {
      paginationSize: 10,
      totalPages: 0,
      currentPage: 0,
      currentSeries: []
    },
    showLoader: false
  },
  getters: {
    getSeries: state => state.series,
    getSerieSelected: state => state.serieSelected,
    getCurrentSeries: state => state.pages.currentSeries,
    getCurrentNumberPage: state => state.pages.currentPage,
    getTotalPages: state => { 
      const remaning = state.series.length % state.pages.paginationSize
      const numberPages = remaning === 0
      ? state.series.length / state.pages.paginationSize
      : Math.floor(state.series.length / state.pages.paginationSize) + 1
      return numberPages
    },
    getShowLoader: state => state.showLoader,
    getPaginationSize: state => state.pages.paginationSize,
  },
  mutations: {
    setSeries: (state, payload) => state.series = payload,
    setSerieSelected: (state, payload) => state.serieSelected = payload,
    setCurrentSeries: (state, payload) => state.pages.currentSeries = payload,
    setCurrentNumberPage: (state, payload) => state.pages.currentPage = payload,
    setTotalPages: (state, payload) => state.pages.totalPages = payload,
    setShowLoader: (state, payload) => state.showLoader = payload,
  },
  actions: {
    allSeries({commit, state}) {
      commit('setShowLoader', true)
      api.getAll()
        .then(succ => {
          commit('setCurrentSeries', succ.slice(0, state.pages.paginationSize))
          commit('setSeries', succ)
          commit('setCurrentNumberPage', succ.length > 0 ? 1 : 0)
          commit('setShowLoader', false)
        })
    },
    querySeries({commit}, payload) {
      api.getQuery(payload)
        .then(succ => {
          const newMovies = succ.map(mov => {
            const {show} = mov
            return {
              name: show.name,
              image: {
                original: show.image?.original ,
                medium: show.image?.medium,
              },
              rating:{
                average: show.rating.average
              },
              schedule: {
                days: show.schedule.days,
                time: show.schedule.time
              },
              runtime: show.runtime,
              id: show.id
            }
          })
          commit('setSeries', newMovies)
          commit('setCurrentSeries', newMovies)
          commit('setCurrentNumberPage', succ.length > 0 ? 1 : 0)
        })
    },
    getDetail({commit}, payload) {
      commit('setShowLoader', true)
      api.getDetail(payload)
        .then(succ => {
          commit('setSerieSelected', succ)
          commit('setShowLoader', false)
        })
    },
    changePage({commit, state}, payload) {
      const initial = state.pages.currentPage * state.pages.paginationSize + 1
      commit('setCurrentNumberPage', payload)
      commit('setCurrentSeries', state.series.slice(initial , initial + state.pages.paginationSize))
    }
  }
})
